@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
package com.atlassian.indexer.model.index;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;