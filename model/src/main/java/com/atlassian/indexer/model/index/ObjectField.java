package com.atlassian.indexer.model.index;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class ObjectField implements IndexField, IndexProperties {
    private final Map<String, IndexField> properties;

    @JsonCreator
    public ObjectField(@JsonProperty("properties") @Nullable final Map<String, IndexField> properties) {
        this.properties = (properties != null) ? properties : new HashMap<>();
    }

    @Override
    public Map<String, IndexField> getProperties() {
        return properties;
    }
}
