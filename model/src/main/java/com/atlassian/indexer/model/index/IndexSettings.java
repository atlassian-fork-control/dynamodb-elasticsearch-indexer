package com.atlassian.indexer.model.index;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nullable;
import java.util.Optional;

public class IndexSettings {
    private static final int DEFAULT_NUMBER_OF_SHARDS = 5;
    private static final int DEFAULT_NUMBER_OF_REPLICAS = 1;

    private final int numberOfShards;
    private final int numberOfReplicas;
    private final Integer maxResultWindow;

    public IndexSettings() {
        this(DEFAULT_NUMBER_OF_SHARDS, DEFAULT_NUMBER_OF_REPLICAS, null);
    }

    @JsonCreator
    public IndexSettings(@JsonProperty("number_of_shards") final int numberOfShards,
                         @JsonProperty("number_of_replicas") final int numberOfReplicas,
                         @JsonProperty("max_result_window") @Nullable final Integer maxResultWindow) {
        this.numberOfShards = numberOfShards;
        this.numberOfReplicas = numberOfReplicas;
        this.maxResultWindow = maxResultWindow;
    }

    public int getNumberOfShards() {
        return numberOfShards;
    }

    public int getNumberOfReplicas() {
        return numberOfReplicas;
    }

    public Optional<Integer> getMaxResultWindow() {
        return Optional.ofNullable(maxResultWindow);
    }
}
