package com.atlassian.indexer.model.index;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class Index {
    private final IndexSettings settings;
    private final Map<String, IndexMapping> mappings;

    @JsonCreator
    public Index(@JsonProperty("settings") @Nullable final IndexSettings settings,
                 @JsonProperty("mappings") @Nullable final Map<String, IndexMapping> mappings) {
        this.settings = (settings != null) ? settings : new IndexSettings();
        this.mappings = (mappings != null) ? mappings : new HashMap<>();
    }

    public IndexSettings getSettings() {
        return settings;
    }

    public Map<String, IndexMapping> getMappings() {
        return mappings;
    }
}
