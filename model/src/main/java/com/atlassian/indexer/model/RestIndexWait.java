package com.atlassian.indexer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.Map;

/**
 * Index wait model.
 */
@Value.Immutable
@Value.Style(init = "with*", get = {"is*", "get*"})
@JsonDeserialize(builder = ImmutableRestIndexWait.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public interface RestIndexWait {
    Map<String, String> getFields();

    Integer getCount();

    Long getTimeoutMillis();
}
