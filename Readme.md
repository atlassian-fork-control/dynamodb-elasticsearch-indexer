DynamoDB Elasticsearch indexer
==============================

An indexer for indexing DynamoDB tables in Elasticsearch.

The indexer is packaged as a Docker image and is intended to be run as a [sidecar](https://docs.microsoft.com/en-us/azure/architecture/patterns/sidecar) to the
primary application.

The Docker image is available on Dockerhub at: https://hub.docker.com/r/atlassian/dynamodb-elasticsearch-indexer 

Usage
=====

On startup, the application instructs the indexer to index a DynamoDB table via a `POST` request to `/rest/indexes`:

    {
      "tableName": "customer"
      "indexName": "customer",
      "streamCheckpointTableName": "customer_checkpoint",
      "index": {
        "settings": {
          "number_of_shards": 3,
          "number_of_replicas": 1
        },
        "mappings": {
          "customer": {
            "_all": {
              "enabled": "false"
            },
            "properties": {
              "id": {
                "type": "keyword"
              }
            }
          }
        }
      }
    } 

The indexer will create the index in Elasticsearch and begin a 2 stage process to index the table:

1) The indexer performs a one-time scan to index existing items in the table.
2) It then attaches to the table's stream to keep the index in sync with changes in the table.

*Note that that scan must complete within the data retention limit of the stream (24 hours by default) to ensure all items are properly indexed.*

![Indexer architecture overview](indexer.png)

The indexer can index multiple tables but only supports a 1:1 mapping between DynamoDB tables and Elasticsearch indexes.

Document properties in the index are directly mapped to DynamoDB item attribute names but may be a subset of the available attributes.  

The indexer is highly available and utilises a lease/checkpoint strategy during both scan and stream stages to ensure only one sidecar instance is actively
indexing a particular index at any one time. It uses a single scan checkpoint table defined in the project's yml configuration for all indexes. Each index
also defines it's own stream checkpoint table in the request payload to create/start the index. These tables must exist prior to starting the indexer.

Configuration
=============

Indexer Options
---------------

__AWS_REGION:__ The AWS region the DynamoDB tables and Elasticsearch cluster are hosted in.

__ELASTICSEARCH_URL:__ The URL to your elastic search cluster.

__SCAN_CHECKPOINT_TABLE_NAME:__ The name of the shared scan checkpoint DynamoDB table. This table must exist prior to starting the indexer. Default 'scan-checkpoint'.

__DYNAMODB_ENDPOINT:__ Optionally set the endpoint in the DynamoDB client. Used for testing, if not provided the AWS region is used.  

Index Options
-------------

__tableName:__ The name of the DynamoDB table to index. This table must exist prior to starting the indexer.

__indexName:__ The name of the Elasticsearch index. This index will be created by the indexer.

__streamCheckpointTableName:__ The name of the stream checkpoint DynamoDB table. This table must exist prior to starting the indexer.

__documentIdAttribute:__ The name of the DynamoDB item attribute to use as the document id in the index. Optional. Defaults to the table's primary key.

__versionAttribute:__ The name of the DynamoDB item attribute to use as the index document version. Optional. If not specified elasticsearch will use internal versioning.

__versionType:__ The versioning type of the index e.g. "external". Optional. See https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-index_.html#index-versioning
     
__index:__ The Elasticsearch index definition (settings and mappings). See https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html

Build
=====

Prerequisites: Maven 3, JDK 8

    mvn clean install

To manually start ElasticSearch locally for development and testing, run:

    echo "xpack.security.enabled: false" > elasticsearch.yml && docker run --rm -p 9200:9200 -e "http.host=0.0.0.0" -e "transport.host=127.0.0.1" -v $(pwd)/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml docker.elastic.co/elasticsearch/elasticsearch:5.3.3

Launch
======

Starting the indexer with the default test configuration:

    docker run -p 9001:8080 atlassian/dynamodb-elasticsearch-indexer:<version> test.yml

Starting the indexer with custom configuration:
    
    docker run
        -p 9001:8080
        -e AWS_REGION us-west-1
        -e ELASTICSEARCH_URL https://elasticsearch.us-west-1.es.amazonaws.com
    atlassian/dynamodb-elasticsearch-indexer:<version> config.yml

Contributors
============

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
=======

Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
