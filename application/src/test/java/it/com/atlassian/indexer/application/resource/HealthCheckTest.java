package it.com.atlassian.indexer.application.resource;

import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import io.dropwizard.testing.junit.DropwizardAppRule;
import it.com.atlassian.indexer.application.AllTests;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.assertj.core.api.Assertions.assertThat;

public class HealthCheckTest {
    @ClassRule
    public static final DropwizardAppRule<IndexerConfiguration> SERVICE = AllTests.SERVICE;

    private static Client client;

    @BeforeClass
    public static void setupClass() {
        client = ClientBuilder.newClient();
    }

    @Test
    public void testBasicWiring() {
        final Response response = client
                .target(String.format("http://localhost:%d", SERVICE.getLocalPort()))
                .path("admin")
                .path("healthcheck")
                .request()
                .get();
        assertThat(response.getStatus()).isEqualTo(SC_OK);
    }
}
