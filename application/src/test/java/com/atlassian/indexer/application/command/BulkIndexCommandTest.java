package com.atlassian.indexer.application.command;

import com.atlassian.elasticsearch.client.Client;
import com.atlassian.elasticsearch.client.document.BulkActionBuilder;
import com.atlassian.elasticsearch.client.document.BulkDeleteActionBuilder;
import com.atlassian.elasticsearch.client.document.BulkIndexActionBuilder;
import com.atlassian.elasticsearch.client.document.BulkRequestBuilder;
import com.atlassian.elasticsearch.client.document.BulkResponse;
import com.atlassian.elasticsearch.client.request.RequestBuilder;
import com.atlassian.indexer.application.entity.BooleanField;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.entity.NumberField;
import com.atlassian.indexer.application.entity.ObjectField;
import com.atlassian.indexer.application.entity.StringField;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import static com.atlassian.elasticsearch.client.ES.bulkDelete;
import static com.atlassian.elasticsearch.client.ES.bulkIndex;
import static com.atlassian.elasticsearch.client.ES.index;
import static com.atlassian.elasticsearch.client.ES.objectContent;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BulkIndexCommandTest {
    @Mock
    private Client searchClient;

    @Mock
    private CompletableFuture<BulkResponse> future;

    @Mock
    private BulkResponse response;

    @Before
    public void setup() {
        when(searchClient.execute(Mockito.<RequestBuilder<BulkResponse>>any())).thenReturn(future);
        when(future.join()).thenReturn(response);
        when(response.isStatusSuccess()).thenReturn(true);
    }

    @Test
    public void run() throws Exception {
        final String indexName = "TestIndex";
        final Map<String, Set<Document>> documentMap = ImmutableMap.<String, Set<Document>>builder()
                .put("typeA", Sets.newHashSet(
                        Document.builder("doc1", "typeA", Document.Operation.PUT).addField(new StringField("f1", "v1")).build(),
                        Document.builder("doc2", "typeA", Document.Operation.DELETE).build()))
                .put("typeB", Sets.newHashSet(
                        Document.builder("doc3", "typeB", Document.Operation.PUT).addField(new BooleanField("f2", true)).build(),
                        Document.builder("doc4", "typeB", Document.Operation.PUT).addField(ObjectField.builder("f3")
                                .addField(new NumberField("f4", 4)).build()).build()))
                .build();

        final CommandResult<Void> result = new BulkIndexCommand(searchClient, indexName, false, documentMap).execute();

        assertThat(result.isError()).isFalse();

        final BulkIndexActionBuilder source1 = bulkIndex().type("typeA").id("doc1").source(objectContent().with("f1", "v1"));
        final BulkDeleteActionBuilder source2 = bulkDelete().type("typeA").id("doc2");
        final BulkIndexActionBuilder source3 = bulkIndex().type("typeB").id("doc3").source(objectContent().with("f2", true));
        final BulkIndexActionBuilder source4 = bulkIndex().type("typeB").id("doc4").source(objectContent().with("f3", objectContent().with("f4", 4)));

        final BulkRequestBuilder expectedRequest = index(indexName).bulk()
                .action(source1)
                .action(source2)
                .action(source3)
                .action(source4);

        // We need to assert that all Actions are added
        // We can't rely on the BulkRequestBuilder equal method as it relies on a list under the hood and elements aren't necessarily inserted in order.
        // We can't retrieve the argument through a method as BulkRequestBuilder has no accessor methods
        // We can't modify it as it exists in a library
        // Reflection is the only way to retrieve the list to check it.
        final ArgumentCaptor<BulkRequestBuilder> captor = ArgumentCaptor.forClass(BulkRequestBuilder.class);
        verify(searchClient).execute(captor.capture());
        verify(searchClient).execute(refEq(expectedRequest, "actions"));
        final BulkRequestBuilder actual = captor.getValue();

        final Field field = actual.getClass().getDeclaredField("actions");
        field.setAccessible(true);
        final List<BulkActionBuilder> actions = (List<BulkActionBuilder>) field.get(actual);
        assertThat(actions).containsOnly(source1, source2, source3, source4);
    }
}