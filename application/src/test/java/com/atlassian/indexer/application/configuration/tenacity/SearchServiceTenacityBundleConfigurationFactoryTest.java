package com.atlassian.indexer.application.configuration.tenacity;

import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.yammer.tenacity.core.config.TenacityConfiguration;
import com.yammer.tenacity.core.properties.TenacityPropertyKey;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceTenacityBundleConfigurationFactoryTest {
    @Mock
    private IndexerConfiguration configuration;

    @Mock
    private TenacityConfiguration mockConfig;

    @Before
    public void before() throws Exception {
        when(configuration.getElasticSearchTenacityConfig()).thenReturn(mockConfig);
    }

    @Test
    public void testAllTenacityKeysHaveConfig() throws Exception {
        final TenacityBundleConfigurationFactory factory = new TenacityBundleConfigurationFactory();
        final Map<TenacityPropertyKey, TenacityConfiguration> configs = factory.getTenacityConfigurations(configuration);
        for (final TenacityPropertyKey key : TenacityPropertyKeys.values()) {
            Assert.assertNotNull("No configuration put into the TenacityConfigurationFactory for key:" + key, configs.get(key));
        }
    }
}
