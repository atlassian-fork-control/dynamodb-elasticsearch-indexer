package com.atlassian.indexer.application.task.stream;

import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.atlassian.indexer.application.configuration.IndexConfiguration;

/**
 * Factory for creating stream workers.
 */
public interface StreamWorkerFactory {
    Worker createWorker(String streamArn, String documentIdAttribute, IndexConfiguration indexConfiguration);
}
