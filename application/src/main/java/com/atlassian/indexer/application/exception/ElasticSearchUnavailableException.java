package com.atlassian.indexer.application.exception;

import com.netflix.hystrix.exception.HystrixBadRequestException;

/**
 * Exception that should be thrown when Elastic Search is (temporarily unavailable).
 * Since this extends {@link HystrixBadRequestException}, it will actually fail the command.
 */
public class ElasticSearchUnavailableException extends HystrixBadRequestException {
    public ElasticSearchUnavailableException(final String message) {
        super(message);
    }
}
