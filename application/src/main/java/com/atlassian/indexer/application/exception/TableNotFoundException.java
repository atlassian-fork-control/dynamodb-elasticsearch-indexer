package com.atlassian.indexer.application.exception;

public class TableNotFoundException extends IndexerException {
    public TableNotFoundException(final String message) {
        super(message);
    }
}
