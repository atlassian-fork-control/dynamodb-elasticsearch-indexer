@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
package com.atlassian.indexer.application.task.init;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;