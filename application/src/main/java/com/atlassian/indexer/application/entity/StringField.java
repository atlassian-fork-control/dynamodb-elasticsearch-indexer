package com.atlassian.indexer.application.entity;

public class StringField extends Field<String> {
    public StringField(final String name, final String value) {
        super(name, value);
    }
}
