package com.atlassian.indexer.application.dao;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ResourceInUseException;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.atlassian.indexer.application.entity.Lease;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Optional;

/**
 * Implementation of the ItemDao.
 */
@Component
public final class LeaseDaoImpl implements LeaseDao {
    private static final Logger logger = LoggerFactory.getLogger(LeaseDaoImpl.class);

    private static final String LEASE_KEY_ATTRIBUTE = "leaseKey";
    private static final String CHECKPOINT_ATTRIBUTE = "checkpoint";
    private static final String COMPLETE_ATTRIBUTE = "complete";
    private static final String EXPIRY_ATTRIBUTE = "expiry";
    private static final String VERSION_ATTRIBUTE = "version";

    private static final long READ_CAPACITY_UNITS = 5;
    private static final long WRITE_CAPACITY_UNITS = 5;

    private final DynamoDB dynamoDb;
    private final String tableName;

    @Autowired
    public LeaseDaoImpl(final DynamoDB dynamoDb, final IndexerConfiguration config) {
        this.dynamoDb = dynamoDb;
        this.tableName = config.getScanCheckpointTableName();

        createSchema();
    }

    @Override
    public Optional<Lease> get(final String key) {
        return Optional.ofNullable(dynamoDb.getTable(tableName).getItem(LEASE_KEY_ATTRIBUTE, key)).map(this::itemToLease);
    }

    @Override
    public Lease put(final Lease lease) {
        final Lease newLease = Lease.builder(lease).setVersion(lease.getVersion() + 1).build();

        final Item item = new Item()
                .withString(LEASE_KEY_ATTRIBUTE, newLease.getKey())
                .withBoolean(COMPLETE_ATTRIBUTE, newLease.isComplete())
                .withString(EXPIRY_ATTRIBUTE, newLease.getExpiry().toString())
                .withLong(VERSION_ATTRIBUTE, newLease.getVersion());

        if (newLease.getCheckpoint() != null) {
            item.withJSON(CHECKPOINT_ATTRIBUTE, DaoJSONUtil.writeValueAsString(newLease.getCheckpoint()));
        }

        final PutItemSpec putItemSpec = new PutItemSpec()
                .withConditionExpression("attribute_not_exists(#leaseKey) or (#version = :version)")
                .withNameMap(new NameMap()
                        .with("#leaseKey", LEASE_KEY_ATTRIBUTE)
                        .with("#version", VERSION_ATTRIBUTE))
                .withValueMap(new ValueMap()
                        .with(":version", lease.getVersion()))
                .withReturnValues(ReturnValue.NONE)
                .withItem(item);

        dynamoDb.getTable(tableName).putItem(putItemSpec);

        return newLease;
    }

    private Lease itemToLease(final Item item) {
        final Lease.Builder lease = Lease.builder(item.getString(LEASE_KEY_ATTRIBUTE))
                .setComplete(item.getBoolean(COMPLETE_ATTRIBUTE))
                .setExpiry(OffsetDateTime.parse(item.getString(EXPIRY_ATTRIBUTE)))
                .setVersion(item.getLong(VERSION_ATTRIBUTE));

        if (item.getJSON(CHECKPOINT_ATTRIBUTE) != null) {
            lease.setCheckpoint(DaoJSONUtil.readValue(item.getJSON(CHECKPOINT_ATTRIBUTE), new TypeReference<Map<String, AttributeValue>>() { }));
        }

        return lease.build();
    }

    private void createSchema() {
        try {
            dynamoDb.getTable(tableName).describe();

            logger.debug("Table {} already exists.", tableName);
        } catch (final ResourceNotFoundException tableNotFound) {
            logger.debug("Creating table {}.", tableName);

            try {
                dynamoDb.createTable(
                        new CreateTableRequest()
                                .withTableName(tableName)
                                .withAttributeDefinitions(
                                        new AttributeDefinition().withAttributeName(LEASE_KEY_ATTRIBUTE).withAttributeType("S"))
                                .withKeySchema(
                                        new KeySchemaElement().withAttributeName(LEASE_KEY_ATTRIBUTE).withKeyType(KeyType.HASH))
                                .withProvisionedThroughput(
                                        new ProvisionedThroughput().withReadCapacityUnits(READ_CAPACITY_UNITS).withWriteCapacityUnits(WRITE_CAPACITY_UNITS)));
            } catch (final ResourceInUseException tableFound) {
                logger.debug("Table {} already exists.", tableName);
            } finally {
                logger.debug("Table {} created successfully.", tableName);
            }
        }
    }
}
