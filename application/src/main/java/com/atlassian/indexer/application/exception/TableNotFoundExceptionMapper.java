package com.atlassian.indexer.application.exception;

import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Exception mapper for {@link TableNotFoundException}.
 *
 * It's expected that the caller will create the table so this mapper assumes
 * that the table is in the process of being created but isn't available yet,
 * hence the 503 response.
 */
@Component
public class TableNotFoundExceptionMapper implements ExceptionMapper<TableNotFoundException> {
    @Override
    public Response toResponse(final TableNotFoundException exception) {
        return Response.status(Response.Status.SERVICE_UNAVAILABLE)
            .type(MediaType.TEXT_PLAIN)
            .entity(exception.getMessage())
            .build();
    }
}
