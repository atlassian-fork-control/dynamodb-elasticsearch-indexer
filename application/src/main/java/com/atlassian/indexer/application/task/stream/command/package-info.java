@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
package com.atlassian.indexer.application.task.stream.command;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;