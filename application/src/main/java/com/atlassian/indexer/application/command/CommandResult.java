package com.atlassian.indexer.application.command;

import com.atlassian.indexer.application.ErrorKeys.ErrorKey;

/**
 * The result of a command. This class can represent a successful or failed response.
 * A successful result may contain a result object. A failed response will always contain
 * an error object which contains an error key, error message and error arguments.
 *
 * @param <T> The type of the result object.
 */
public final class CommandResult<T> {
    private ErrorKey errorKey;
    private String errorMessage;
    private T result;

    private CommandResult(final ErrorKey errorKey, final String errorMessage) {
        this.errorKey = errorKey;
        this.errorMessage = errorMessage;
    }

    private CommandResult(final T result) {
        this.result = result;
    }

    private CommandResult() {
    }

    public static <T> CommandResult<T> error(final ErrorKey errorKey, final String errorMessage) {
        return new CommandResult<>(errorKey, errorMessage);
    }

    public static <T> CommandResult<T> result(final T result) {
        return new CommandResult<>(result);
    }

    public static <T> CommandResult<T> ok() {
        return new CommandResult<>();
    }

    public boolean isError() {
        return errorKey != null;
    }

    public ErrorKey getErrorKey() {
        return errorKey;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public T getResult() {
        return result;
    }
}
