package com.atlassian.indexer.application.task;

import com.atlassian.indexer.application.configuration.IndexConfiguration;

/**
 * Factory that creates index tasks.
 */
public interface IndexTaskFactory {
    /**
     * Creates a task for the given configuration that indexes a dynamo table in Elasticsearch.
     *
     * @param indexConfiguration The index configuration.
     * @return a new index task.
     */
    IndexTask createIndexTask(IndexConfiguration indexConfiguration);
}
