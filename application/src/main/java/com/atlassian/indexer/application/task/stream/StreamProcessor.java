package com.atlassian.indexer.application.task.stream;

import com.amazonaws.services.dynamodbv2.model.Record;
import com.amazonaws.services.dynamodbv2.streamsadapter.model.RecordAdapter;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.ShutdownReason;
import com.amazonaws.services.kinesis.clientlibrary.types.InitializationInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ProcessRecordsInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownInput;
import com.atlassian.indexer.application.exception.ElasticSearchUnavailableException;
import com.atlassian.indexer.application.task.stream.command.CheckpointShardCommand;
import com.atlassian.indexer.application.task.stream.command.ProcessRecordsCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class StreamProcessor implements IRecordProcessor {
    private static final Logger logger = LoggerFactory.getLogger(StreamProcessor.class);

    private final CheckpointShardCommand checkpointShardCommand;
    private final ProcessRecordsCommand processRecordsCommand;
    private final String indexName;

    private String shardId;

    public StreamProcessor(final CheckpointShardCommand checkpointShardCommand,
                           final ProcessRecordsCommand processRecordsCommand,
                           final String indexName) {
        this.checkpointShardCommand = checkpointShardCommand;
        this.processRecordsCommand = processRecordsCommand;
        this.indexName = indexName;
    }

    @Override
    public void initialize(final InitializationInput initializationInput) {
        this.shardId = initializationInput.getShardId();
        logger.info("Index {}: Initializing record processor for shard: {}", indexName, shardId);
    }

    @Override
    public void processRecords(final ProcessRecordsInput processRecordsInput) {
        final List<Record> records = processRecordsInput.getRecords().stream()
                .filter(record -> record instanceof RecordAdapter)
                .map(record -> ((RecordAdapter) record).getInternalObject())
                .collect(Collectors.toList());

        try {
            processRecordsCommand.execute(new ProcessRecordsCommand.ProcessRecordsCommandInput(shardId, records));
        } catch (final ElasticSearchUnavailableException ex) {
            logger.error("Index {}: Failed to process records, not updating shard", indexName, ex);
            throw ex;
        }

        logger.info("Index {}: Checkpointing shard: {}", indexName, shardId);
        checkpointShardCommand.execute(processRecordsInput.getCheckpointer());
    }

    @Override
    public void shutdown(final ShutdownInput shutdownInput) {
        logger.info("Index {}: Shutting down record processor for shard: {}", indexName, shardId);
        // Important to checkpoint after reaching end of shard, so we can start processing data from child shards.
        if (shutdownInput.getShutdownReason() == ShutdownReason.TERMINATE) {
            logger.info("Index {}: Checkpointing shard: {}", indexName, shardId);
            checkpointShardCommand.execute(shutdownInput.getCheckpointer());
        }
    }
}