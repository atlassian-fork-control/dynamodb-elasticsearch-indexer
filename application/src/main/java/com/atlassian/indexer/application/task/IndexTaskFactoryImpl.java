package com.atlassian.indexer.application.task;

import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.dao.ItemDao;
import com.atlassian.indexer.application.dao.ItemDaoFactory;
import com.atlassian.indexer.application.service.IndexService;
import com.atlassian.indexer.application.service.LeaseService;
import com.atlassian.indexer.application.task.init.InitialiseIndexTask;
import com.atlassian.indexer.application.task.scan.ItemsToDocumentMapFunction;
import com.atlassian.indexer.application.task.scan.ScanIndexTask;
import com.atlassian.indexer.application.task.stream.StreamIndexTask;
import com.atlassian.indexer.application.task.stream.StreamWorkerFactory;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class IndexTaskFactoryImpl implements IndexTaskFactory {
    private final ItemDaoFactory itemDaoFactory;
    private final LeaseService leaseService;
    private final IndexService indexService;
    private final StreamWorkerFactory streamWorkerFactory;

    @Autowired
    public IndexTaskFactoryImpl(final ItemDaoFactory itemDaoFactory,
                                final LeaseService leaseService,
                                final IndexService indexService,
                                final StreamWorkerFactory streamWorkerFactory) {
        this.itemDaoFactory = itemDaoFactory;
        this.leaseService = leaseService;
        this.indexService = indexService;
        this.streamWorkerFactory = streamWorkerFactory;
    }

    @Override
    public IndexTask createIndexTask(final IndexConfiguration indexConfiguration) {
        final ItemDao itemDao = itemDaoFactory.createItemDao(indexConfiguration.getTableName());
        final TableDescription tableDescription = itemDao.getTableDescription();
        final String documentIdAttribute = getDocumentIdAttribute(indexConfiguration, tableDescription);
        final ItemToDocumentsFunction itemToDocumentsFunction = ImmutableItemToDocumentsFunction.builder()
                .withIndex(indexConfiguration.getIndex())
                .withDocumentIdAttribute(documentIdAttribute)
                .withVersionAttribute(indexConfiguration.getVersionAttribute())
                .withVersionType(indexConfiguration.getVersionType())
                .build();
        final ItemsToDocumentMapFunction itemsToDocumentMapFunction = new ItemsToDocumentMapFunction(itemToDocumentsFunction);
        final Worker worker = streamWorkerFactory.createWorker(tableDescription.getLatestStreamArn(), documentIdAttribute, indexConfiguration);

        return new IndexTaskImpl(Lists.newArrayList(
                new InitialiseIndexTask(indexConfiguration, indexService),
                new ScanIndexTask(itemDao, leaseService, indexConfiguration, indexService, itemsToDocumentMapFunction),
                new StreamIndexTask(worker)));
    }

    private String getDocumentIdAttribute(final IndexConfiguration indexConfiguration, final TableDescription tableDescription) {
        return indexConfiguration.getDocumentIdAttribute()
                .orElse(getFirstMatching(tableDescription.getKeySchema(), KeyType.RANGE)
                        .orElse(getFirstMatching(tableDescription.getKeySchema(), KeyType.HASH)
                                .orElseThrow(() -> new IllegalStateException(String.format("Index %s: Table %s missing key schema",
                                        indexConfiguration.getIndexName(), indexConfiguration.getTableName())))));
    }

    private Optional<String> getFirstMatching(final List<KeySchemaElement> keySchema, final KeyType keyType) {
        return keySchema.stream()
                .filter(keySchemaElement -> KeyType.fromValue(keySchemaElement.getKeyType()) == keyType)
                .map(KeySchemaElement::getAttributeName)
                .findFirst();
    }
}
