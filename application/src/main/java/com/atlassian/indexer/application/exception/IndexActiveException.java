package com.atlassian.indexer.application.exception;

public class IndexActiveException extends IndexerException {
    public IndexActiveException(final String message) {
        super(message);
    }
}
