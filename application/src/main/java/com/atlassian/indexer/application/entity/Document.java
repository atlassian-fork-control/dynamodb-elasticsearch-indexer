package com.atlassian.indexer.application.entity;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public final class Document {
    public enum Operation {
        DELETE, PUT
    }

    private final String id;
    private final String type;
    private final Operation operation;
    private final Set<Field<?>> fields;
    private final Long version;
    private final String versionType;

    private Document(final Builder builder) {
        this.id = builder.id;
        this.type = builder.type;
        this.operation = builder.operation;
        this.fields = builder.fields;
        this.version = builder.version;
        this.versionType = builder.versionType;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Operation getOperation() {
        return operation;
    }

    public Set<Field<?>> getFields() {
        return fields;
    }

    public Optional<Long> getVersion() {
        return Optional.ofNullable(version);
    }

    public Optional<String> getVersionType() {
        return Optional.ofNullable(versionType);
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Document document = (Document) o;
        return Objects.equals(id, document.id) &&
                Objects.equals(type, document.type) &&
                operation == document.operation &&
                Objects.equals(fields, document.fields) &&
                Objects.equals(version, document.version) &&
                Objects.equals(versionType, document.versionType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, operation, fields, version, versionType);
    }

    @Override
    public String toString() {
        return "Document{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", operation=" + operation +
                ", fields=" + fields +
                ", version=" + version +
                ", versionType='" + versionType + '\'' +
                '}';
    }

    public static Builder builder(final String id, final String type, final Operation operation) {
        return new Builder(id, type, operation);
    }

    public static final class Builder implements FieldBuilder<Builder> {
        private final String id;
        private final String type;
        private final Operation operation;
        private final Set<Field<?>> fields;
        private Long version;
        private String versionType;

        private Builder(final String id, final String type, final Operation operation) {
            this.id = id;
            this.type = type;
            this.operation = operation;
            this.fields = new HashSet<>();
        }

        @Override
        public Builder addField(final Field field) {
            fields.add(field);
            return this;
        }

        public Builder setVersion(final Long version) {
            this.version = version;
            return this;
        }

        public Builder setVersionType(final String versionType) {
            this.versionType = versionType;
            return this;
        }

        public Document build() {
            return new Document(this);
        }
    }
}
