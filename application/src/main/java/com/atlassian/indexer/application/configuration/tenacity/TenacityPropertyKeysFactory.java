package com.atlassian.indexer.application.configuration.tenacity;

import com.yammer.tenacity.core.properties.TenacityPropertyKey;
import com.yammer.tenacity.core.properties.TenacityPropertyKeyFactory;

/**
 * {@link TenacityPropertyKeyFactory} that converts {@link String}s to their associated {@link TenacityPropertyKey}.
 */
public class TenacityPropertyKeysFactory implements TenacityPropertyKeyFactory {
    @Override
    public TenacityPropertyKey from(final String s) {
        return TenacityPropertyKeys.valueOf(s);
    }
}
