package com.atlassian.indexer.application.entity;

import com.google.common.base.Joiner;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public final class ObjectField extends Field<Set<Field<?>>> {
    private ObjectField(final Builder builder) {
        super(builder.name, builder.fields);
    }

    @Override
    public Stream<Map.Entry<String, ?>> flatten(@Nullable final String prefix) {
        return getValue().stream().flatMap(field -> field.flatten(Joiner.on('.').skipNulls().join(prefix, getName())));
    }

    public static Builder builder(final String name) {
        return new Builder(name);
    }

    public static final class Builder implements FieldBuilder<Builder> {
        private final String name;
        private final Set<Field<?>> fields;

        private Builder(final String name) {
            this.name = name;
            this.fields = new HashSet<>();
        }

        @Override
        public Builder addField(final Field field) {
            fields.add(field);
            return this;
        }

        public ObjectField build() {
            return new ObjectField(this);
        }
    }
}
