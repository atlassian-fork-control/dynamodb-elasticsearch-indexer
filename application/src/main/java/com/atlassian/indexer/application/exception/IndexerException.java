package com.atlassian.indexer.application.exception;

public class IndexerException extends RuntimeException {
    public IndexerException(final String message) {
        super(message);
    }
}
