package com.atlassian.indexer.application.task.stream.command;

import com.amazonaws.services.kinesis.clientlibrary.exceptions.InvalidStateException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ShutdownException;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class CheckpointShardCommand extends RetryableCommand<IRecordProcessorCheckpointer> {
    private static final Logger logger = LoggerFactory.getLogger(CheckpointShardCommand.class);

    private static final int NUMBER_OF_ATTEMPTS = 10;
    private static final long BACKOFF_TIME_IN_MILLIS = TimeUnit.SECONDS.toMillis(3);

    public CheckpointShardCommand() {
        super(NUMBER_OF_ATTEMPTS, BACKOFF_TIME_IN_MILLIS);
    }

    @Override
    protected void run(final IRecordProcessorCheckpointer checkpointer) {
        try {
            checkpointer.checkpoint();
        } catch (InvalidStateException e) {
            // This indicates an issue with the DynamoDB table (check for table, provisioned IOPS).
            logger.error("Cannot save checkpoint to the DynamoDB table used by the Amazon Kinesis Client Library.", e);
        } catch (ShutdownException e) {
            // Ignore checkpoint if the processor instance has been shutdown (fail over).
            logger.info("Caught shutdown exception, skipping checkpoint.", e);
        }
    }

    @Override
    protected void error(final IRecordProcessorCheckpointer checkpointer, final int attempt, final Exception e) {
        logger.warn("Checkpoint error on attempt {} of {}, cause: {}", attempt, numberOfAttempts, e.getMessage(), e);
    }

    @Override
    protected void fail(final IRecordProcessorCheckpointer checkpointer) {
        logger.error("Checkpoint failed after {} attempts.", numberOfAttempts);
    }
}
