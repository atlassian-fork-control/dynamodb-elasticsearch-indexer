package com.atlassian.indexer.application.command;

import com.atlassian.elasticsearch.client.Client;
import com.atlassian.elasticsearch.client.indices.AbstractPropertiesFieldBuilder;
import com.atlassian.elasticsearch.client.indices.CreateIndexRequestBuilder;
import com.atlassian.elasticsearch.client.indices.CreateIndexResponse;
import com.atlassian.elasticsearch.client.indices.CreateIndexSourceBuilder;
import com.atlassian.elasticsearch.client.indices.IndexExistsResponse;
import com.atlassian.elasticsearch.client.indices.IndexSettingsBuilder;
import com.atlassian.elasticsearch.client.indices.MappingBuilder;
import com.atlassian.elasticsearch.client.indices.ObjectFieldBuilder;
import com.atlassian.indexer.application.configuration.tenacity.TenacityPropertyKeys;
import com.atlassian.indexer.model.index.AllSetting;
import com.atlassian.indexer.model.index.BooleanField;
import com.atlassian.indexer.model.index.DateField;
import com.atlassian.indexer.model.index.Index;
import com.atlassian.indexer.model.index.IndexField;
import com.atlassian.indexer.model.index.IndexMapping;
import com.atlassian.indexer.model.index.IndexSettings;
import com.atlassian.indexer.model.index.IntegerField;
import com.atlassian.indexer.model.index.KeywordField;
import com.atlassian.indexer.model.index.LongField;
import com.atlassian.indexer.model.index.ObjectField;
import com.atlassian.indexer.model.index.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.function.BiFunction;

import static com.atlassian.elasticsearch.client.ES.booleanField;
import static com.atlassian.elasticsearch.client.ES.createIndexSource;
import static com.atlassian.elasticsearch.client.ES.dateField;
import static com.atlassian.elasticsearch.client.ES.index;
import static com.atlassian.elasticsearch.client.ES.indexSettings;
import static com.atlassian.elasticsearch.client.ES.integerField;
import static com.atlassian.elasticsearch.client.ES.keywordField;
import static com.atlassian.elasticsearch.client.ES.longField;
import static com.atlassian.elasticsearch.client.ES.mapping;
import static com.atlassian.elasticsearch.client.ES.objectContent;
import static com.atlassian.elasticsearch.client.ES.objectField;
import static com.atlassian.elasticsearch.client.ES.textField;
import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_INDEX_CREATION_FAILED;

/**
 * A command to create an index in elasticsearch.
 */
public class CreateIndexCommand extends AbstractSearchCommand<Void> {
    private static final Logger logger = LoggerFactory.getLogger(CreateIndexCommand.class);

    private static final String INDEX_ALREADY_EXISTS_EXCEPTION = "index_already_exists_exception";

    private static final BiFunction<String, Index, CreateIndexRequestBuilder> TO_CREATE_INDEX_REQUEST_BUILDER = (indexName, index) -> {
        final CreateIndexSourceBuilder indexSource = createIndexSource();

        final IndexSettings settings = index.getSettings();

        logger.debug("adding settings to index '{}' (replicas: '{}', shards: '{}')", indexName, settings.getNumberOfReplicas(), settings.getNumberOfShards());
        addSettingsToIndexSource(settings, indexSource);

        index.getMappings().forEach((type, mapping) -> {
            logger.debug("adding mapping to index for type '{}'", type);
            addMappingToIndexSource(indexSource, type, mapping);
        });

        return index(indexName).create().source(indexSource);
    };

    private final Client searchClient;
    private final String indexName;
    private final Index index;

    public CreateIndexCommand(final Client searchClient, final String indexName, final Index index) {
        super(TenacityPropertyKeys.CREATE_INDEX);

        this.searchClient = searchClient;
        this.indexName = indexName;
        this.index = index;
    }

    @Override
    protected CommandResult<Void> run() {
        logger.debug("Executing index exists command for index {}", indexName);
        final IndexExistsResponse indexExistsResponse = searchClient.execute(index(indexName).exists()).join();
        logger.debug("Index exists response received for index {}", indexName);

        if (!indexExistsResponse.indexExists()) {
            final CreateIndexRequestBuilder source = TO_CREATE_INDEX_REQUEST_BUILDER.apply(indexName, index);

            logger.debug("Executing index creation command for index {}", indexName);
            final CreateIndexResponse createIndexResponse = searchClient.execute(source).join();
            logger.debug("Finished executing index creation command for index {}", indexName);

            if (!createIndexResponse.isStatusSuccess()) {
                logger.debug("Failed to create index {}: {}", indexName, createIndexResponse.toString());
                final boolean indexAlreadyExists = createIndexResponse.getContent()
                        .getString("type")
                        .map(type -> type.equalsIgnoreCase(INDEX_ALREADY_EXISTS_EXCEPTION))
                        .orElse(false);

                if (!indexAlreadyExists) {
                    return CommandResult.error(ELASTICSEARCH_INDEX_CREATION_FAILED, "Could not create the index in elastic search");
                } else {
                    logger.debug("Index {} exists, skipping creation", indexName);
                }
            }
        } else {
            logger.debug("Index {} exists, skipping creation", indexName);
        }

        return CommandResult.ok();
    }

    private static void addSettingsToIndexSource(final IndexSettings settings, final CreateIndexSourceBuilder indexSource) {

        final IndexSettingsBuilder indexSettingsBuilder = indexSettings()
                .numberOfShards(settings.getNumberOfShards())
                .numberOfReplicas(settings.getNumberOfReplicas())
                .setting("index.mapper.dynamic", "false");

        settings.getMaxResultWindow()
                .ifPresent(maxResultWindow -> indexSettingsBuilder.setting("max_result_window", String.valueOf(maxResultWindow)));

        indexSource.settings(indexSettingsBuilder);
    }

    private static void addMappingToIndexSource(final CreateIndexSourceBuilder indexSource, final String type, final IndexMapping mapping) {

        final MappingBuilder mappingBuilder = mapping();

        mapping.getAllSetting()
                .map(AllSetting::isEnabled)
                .ifPresent(allSetting -> {
                    logger.debug("adding all setting '{}' to mapping '{}'", allSetting, type);
                    mappingBuilder.setting("_all", objectContent().with("enabled", allSetting));
                });

        addFieldsToBuilder(type, mappingBuilder, mapping.getProperties());

        indexSource.mapping(type, mappingBuilder);
    }

    private static void addFieldsToBuilder(
            final String builderName,
            final AbstractPropertiesFieldBuilder<?> fieldBuilder,
            final Map<String, IndexField> properties) {
        properties.forEach((fieldName, field) -> {
            logger.debug("adding field '{}' to fieldBuilder '{}'", fieldName, builderName);
            if (field instanceof DateField) {
                fieldBuilder.field(fieldName, dateField());
            } else if (field instanceof KeywordField) {
                fieldBuilder.field(fieldName, keywordField());
            } else if (field instanceof TextField) {
                fieldBuilder.field(fieldName, textField());
            } else if (field instanceof LongField) {
                fieldBuilder.field(fieldName, longField());
            } else if (field instanceof IntegerField) {
                fieldBuilder.field(fieldName, integerField());
            } else if (field instanceof BooleanField) {
                fieldBuilder.field(fieldName, booleanField());
            } else if (field instanceof ObjectField) {
                final ObjectFieldBuilder objectFieldBuilder = objectField();
                addFieldsToBuilder(fieldName, objectFieldBuilder, ((ObjectField) field).getProperties());
                fieldBuilder.field(fieldName, objectFieldBuilder);
            } else {
                throw new IllegalArgumentException(String.format("Unrecognised index field type %s", field.getClass().getSimpleName()));
            }
        });
    }
}
